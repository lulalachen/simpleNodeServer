const express = require('express')
const morgan = require('morgan')
const fetch = require('isomorphic-fetch')
const bodyParser = require('body-parser')

const port = process.env.PORT || 3000
const API_KEY = process.env.APIKEY
const API_URL = 'https://api.airtable.com/v0/appX2N31o7ZsFILRK/Packs'

const app = express()

app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

app.use(express.static(__dirname + '/her'))

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/her/index.html')
})

app.post('/', (req, res) => {
  console.log(req.body)
  return fetch(API_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${API_KEY}`,
      },
      body: JSON.stringify(req.body),
    })
    .then((response) => response.json())
    .then((jsonData) => res.send(jsonData))
})

app.listen(port, (err) => {
  if (err) {
    console.log(err)
  }

  console.log(`🚧  Server start at http://localhost:${port}`)
})
